package ar.edu.cresta;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import static org.junit.jupiter.api.Assertions.*;

import ar.edu.cresta.dto.ClubFutbolDTO;
import ar.edu.cresta.model.ClubFutbol;
import ar.edu.cresta.repository.ClubFutbolRepository;
import ar.edu.cresta.service.ClubFutbolService;

@DataJpaTest //configura la prueba para utilizar una configuración específica de JPA y proporciona una base de datos en memoria para las pruebas
@AutoConfigureMockMvc //se usa para configurar automáticamente el entorno de la aplicación
public class ClubFutbolServiceIntegracion {

	@Autowired
    private ClubFutbolRepository clubFutbolRepository;

    @Test
    public void testGuardarClubIntegration() {
    	
    	// Configuración manual sin usar @Autowired
        ClubFutbolService clubFutbolService = new ClubFutbolService(clubFutbolRepository);
        
        // Crear un objeto DTO con datos de prueba
        ClubFutbolDTO clubDTO = new ClubFutbolDTO("ClubTest", "EstadioTest", "UbicacionTest");

        // Llamar al método del servicio que interactúa con la base de datos
        ClubFutbol clubGuardado = clubFutbolService.guardarClub(clubDTO);

        // Verificar que el club se ha guardado correctamente en la base de datos
        assertNotNull(clubGuardado.getId());
        
        assertEquals("ClubTest", clubGuardado.getNombre());
        assertEquals("EstadioTest", clubGuardado.getEstadio());
        assertEquals("UbicacionTest", clubGuardado.getUbicacion());

        // Verificar que el club se puede recuperar correctamente de la base de datos
        ClubFutbol clubRecuperado = clubFutbolRepository.findById(clubGuardado.getId()).orElse(null);
        assertNotNull(clubRecuperado);
        assertEquals("ClubTest", clubRecuperado.getNombre());
        assertEquals("EstadioTest", clubRecuperado.getEstadio());
        assertEquals("UbicacionTest", clubRecuperado.getUbicacion());
    }
}
