package ar.edu.cresta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.cresta.dto.ClubFutbolDTO;
import ar.edu.cresta.model.ClubFutbol;
import ar.edu.cresta.repository.ClubFutbolRepository;

import java.util.List;

@Service
public class ClubFutbolService {

	private final ClubFutbolRepository clubFutbolRepository;

	@Autowired
	public ClubFutbolService(ClubFutbolRepository clubFutbolRepository) {
		this.clubFutbolRepository = clubFutbolRepository;
	}

	public List<ClubFutbolDTO> obtenerTodosLosClubes() {
		return clubFutbolRepository.obtenerClubesDTO();
	}

	public ClubFutbol guardarClub(ClubFutbolDTO club_dto) {
		ClubFutbol club = new ClubFutbol(
				club_dto.getNombre(),
				club_dto.getEstadio(),
				club_dto.getUbicacion());
		
        return clubFutbolRepository.save(club);
    }
}
