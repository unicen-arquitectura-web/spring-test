package ar.edu.cresta.model;

import jakarta.persistence.*;

@Entity
@Table(name = "club_futbol")
public class ClubFutbol {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "estadio")
	private String estadio;

	@Column(name = "ubicacion")
	private String ubicacion;

	public ClubFutbol() {
	}

	public ClubFutbol(String nombre, String estadio, String ubicacion) {
		this.nombre = nombre;
		this.estadio = estadio;
		this.ubicacion = ubicacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstadio() {
		return estadio;
	}

	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ClubFutbol [id=" + id + ", nombre=" + nombre + ", estadio=" + estadio + ", ubicacion=" + ubicacion
				+ "]";
	}

}
