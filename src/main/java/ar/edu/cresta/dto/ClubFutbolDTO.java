package ar.edu.cresta.dto;

public class ClubFutbolDTO {

	private String nombre;
	private String estadio;
	private String ubicacion;

	public ClubFutbolDTO() {
	}

	public ClubFutbolDTO(String nombre, String estadio, String ubicacion) {
		this.nombre = nombre;
		this.estadio = estadio;
		this.ubicacion = ubicacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstadio() {
		return estadio;
	}

	public void setEstadio(String estadio) {
		this.estadio = estadio;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	@Override
	public String toString() {
		return "ClubFutbolDTO [nombre=" + nombre + ", estadio=" + estadio + ", ubicacion=" + ubicacion + "]";
	}

}
