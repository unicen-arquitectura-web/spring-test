package ar.edu.cresta.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ar.edu.cresta.dto.ClubFutbolDTO;
import ar.edu.cresta.model.ClubFutbol;
import ar.edu.cresta.service.ClubFutbolService;

import java.util.List;

@RestController
@RequestMapping("/api/clubes_futbol")
public class ClubFutbolController {

	private final ClubFutbolService clubFutbolService;

	@Autowired
	public ClubFutbolController(ClubFutbolService clubFutbolService) {
		this.clubFutbolService = clubFutbolService;
	}

	@GetMapping
	public ResponseEntity<List<ClubFutbolDTO>> obtenerTodosLosClubes() {
		List<ClubFutbolDTO> clubes = clubFutbolService.obtenerTodosLosClubes();
		return new ResponseEntity<>(clubes, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ClubFutbol> crearClub(@RequestBody ClubFutbolDTO ClubFutbolDTO) {
		ClubFutbol clubCreado = clubFutbolService.guardarClub(ClubFutbolDTO);
		return new ResponseEntity<ClubFutbol>(clubCreado, HttpStatus.CREATED);
	}
}
