package ar.edu.cresta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.edu.cresta.dto.ClubFutbolDTO;
import ar.edu.cresta.model.ClubFutbol;

public interface ClubFutbolRepository extends JpaRepository<ClubFutbol, Long> {

	@Query("SELECT new ar.edu.cresta.dto.ClubFutbolDTO(c.nombre, c.estadio, c.ubicacion) FROM ClubFutbol c")
	List<ClubFutbolDTO> obtenerClubesDTO();

}
